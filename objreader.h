#pragma once
#include "gameObject.h"
#include <iostream>
#include <string>
using namespace std;
class objreader
{
private:
	struct tempQuad
	{
		int vertex1, vertex2, vertex3, vertex4;
		int vTexture1, vTexture2, vTexture3, vTexture4;
		int vNormal1, vNormal2, vNormal3, vNormal4;
	};

public:
	objreader();
	~objreader();
	//function reads file at file location passed in, returns a gameobject using data from the file
	gameObject readOBJfile(char * inFile);
};