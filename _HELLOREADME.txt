IF THE PROGRAM COMPLAINS ABOUT glut32.dll BEING MISSING IT'S IN THE SAME DIRECTORY AS THIS. JUST DROP IT BACK IN DEBUG TO SOLVE THE ISSUE.

LOD CONTROLLER + OBJ LOADER

This project containes code to load an object from a .obj file, and a system showcasing a discrete LOD controller.

CONTROLS
USE . TO INCREASE OBJECTS AND , TO DECRASE. MAX 50 \n USE T TO INCREASE LOD THRESHOLDS AND G TO DECREASE \n USE # TO START CAMERA ROTATION \n USE I AND K TO MOVE THE OBJECT TOWARDS AND AWAY!\nUSE J AND L TO MOVE THE OBJECT LEFT AND RIGHT!\nUSE U AND O TO MOVE THE OBJECT UP AND DOWN!\nUSE [ AND ] TO CHANGE SPEED OF OBJECT MOVEMENT!

'.' - Increases number of objects in scene (NOTE: These objects will be placed on top of each other until you use the scatter key)
',' - Decreases number of objects in scene
't' - Increases distance of LOD thresholds
'g' - Decreases distance of LOD thresholds
'#' - Starts camera rotation
'p' - Scatters objects randomly
'i' - move object towards origin/negative direction on Z axis
'k' - moves object away from origin/positive direction on Z axis
'j' - moves object away from origin/positive direction on X axis
'l' - move object towards origin/negative direction on X axis
'u' - moves object away from origin/positive direction on Y axis
'o' - move object towards origin/negative direction on Y axis
'[' - Decreases speed of the movement keys
']' - increases speed of the movement keys