#include "material.h"
#include <string.h>
#include "stdafx.h"
material::material()
{
	kd.R = 0.0f;
	kd.G = 0.0f;
	kd.B = 0.0f;

	ka.R = 0.0f;
	ka.G = 0.0f;
	ka.B = 0.0f;

	tf.R = 0.0f;
	tf.G = 0.0f;
	tf.B = 0.0f;

	Ni = 0.0f;

	strcpy(mtlName, "");
}

material::~material()
{
}
