#pragma once
#include "material.h"
class gameObject
{
private:

public:
	gameObject();
	gameObject(int invSize, int invtSize, int invnSize, int infSize, int inmtlSize);
	~gameObject();
	//stores the number of each type of data
	int vSize;
	int vtSize;
	int vnSize;
	int fSize;
	int mtlSize;
	//structures for each type
	struct Vertex
	{
		float x, y, z;
	};

	struct vTexture
	{
		float x, y;
	};

	struct vNormal 
	{
		float x, y, z;
	};
	/////////////////////////////////////////////////////////////////////////////////
	//TODO: ADAPT FACE STRUCT TO ALLOW FOR TRIS AS WELL                            //
	/////////////////////////////////////////////////////////////////////////////////

	struct Face
	{
		int vertex1, vertex2, vertex3;
		int vTexture1, vTexture2, vTexture3;
		int vNormal1, vNormal2, vNormal3;
		int mtlIndex;
	};
	
	//lists for each data type
	material * mtlList;
	Vertex * vList;
	vTexture * vtList;
	vNormal * vnList;
	Face * fList;
};