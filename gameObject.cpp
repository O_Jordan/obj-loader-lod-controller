#include "gameObject.h"
#include "stdafx.h"
gameObject::gameObject()
{
}
gameObject::gameObject(int invSize, int invtSize, int invnSize, int infSize, int inmtlSize)
{
	//set sizes
	vSize = invSize;
	vtSize = invtSize;
	vnSize = invnSize;
	fSize = infSize;
	mtlSize = inmtlSize;
	//initialise lists
	vList = new Vertex[vSize];
	vtList = new vTexture[vtSize];
	vnList = new vNormal[vnSize];
	fList = new Face[fSize];
	mtlList = new material[mtlSize];
	//fill vertex list with emty vertecies
	for (int i = 0; i < vSize; i++)
	{
		Vertex clean;
		clean.x = 0.0f;
		clean.y = 0.0f;
		clean.z = 0.0f;
		vList[i] = clean;
	}
}

gameObject::~gameObject()
{

}
