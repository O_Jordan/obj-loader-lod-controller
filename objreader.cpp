#include "objreader.h"
#include "gameObject.h"
#include "stdafx.h"
objreader::objreader()
{
}

objreader::~objreader()
{
}

gameObject objreader::readOBJfile(char * inFile)
{
	//ready buffer for fopen
	char  buffer[50];
	//set up file stream
	FILE * objFile;
	//counts for data lines
	int vCount = 0;
	int vtCount = 0;
	int vnCount = 0;
	int fCount = 0;
	int mtlCount = 0;
	int faceCornerCount = 0;
	bool inFace = false;
	bool didRead = false;
	//set up storage for file path
	char mtlfilepath[50];
	//open file
	objFile = fopen(inFile, "r");
	//scan in first line
	fscanf(objFile, "%s", buffer);
	//loop while not at the end of objfile
	while (!feof(objFile))
	{
		//if buffer isn't whitespace
		if (buffer != "")
		{
			//printf(buffer);
			//if buffer matches one of keys, increase respective count
			if (strcmp(buffer, "v") == 0)
			{
				vCount++;
			}
			if (strcmp(buffer, "vt") == 0)
			{
				vtCount++;
			}
			if (strcmp(buffer, "vn") == 0)
			{
				vnCount++;
			}
			if (strcmp(buffer, "f") == 0)
			{
				fCount++;
				if (inFace)
				{
					faceCornerCount = 0;
				}
				else
				{
					inFace = true;
				}
				
			}
			else if (inFace)
			{
				if (strcmp(buffer, "usemtl") == 0 || strcmp(buffer, "g") == 0)
				{
					inFace = false;
					faceCornerCount = 0;
				}
				else if (faceCornerCount == 3)
				{
					fCount++;
					inFace = false;
					faceCornerCount = 0;
				}
				else
				{
					faceCornerCount++;
				}
			}
			if (strcmp(buffer, "usemtl") == 0)
			{
				mtlCount++;
			}
			if (strcmp(buffer, "mtllib") == 0)
			{
				//set up mtl file path from metadata
				fscanf(objFile, "%s", mtlfilepath);
			}
			
		}
		//scan in next line
		fscanf(objFile,"%s", buffer);
		//printf("VCount: %d\nvtCount: %d\nvnCount: %d\nfCount: %d\n", vCount, vtCount, vnCount, fCount);
	}
	//ready internal Game Object
	gameObject object(vCount, vtCount, vnCount, fCount, mtlCount);

	//set up file stream for mtl file
	FILE * mtlFile;

	//return to start of objfile
	rewind(objFile);

	//initialise counts for tracking how many of each type has been stored in object, use it to check against original counts
	int vIn = 0;
	int vtIn = 0;
	int vnIn = 0;
	int fIn = 0;
	int mtlIn = 0;

	//open mtl file
	mtlFile = fopen(mtlfilepath, "r");
	//while not at end of mtlfile
	while (!feof(mtlFile))
	{
		//read in first word
		fscanf(mtlFile, "%s", buffer);
		//if is newmtl, we know that the following however many lines are data pertaining to mtl info
		//TO DO: For consistency, should include checks on data and if strings aren't as expected, should return something like "corrupt"
		if (strcmp(buffer, "newmtl") == 0)
		{
			fscanf(mtlFile, "%s", &object.mtlList[mtlIn].mtlName);
			//should in thoery be passing through "illum" no need to use it, fscanfing again afterit to get actual value
			fscanf(mtlFile, "%s", buffer);
			//actual value as atring
			fscanf(mtlFile, "%d", &object.mtlList[mtlIn].illum);
			//next through to name of value
			fscanf(mtlFile, "%s", buffer);
			//actual values
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].kd.R);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].kd.G);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].kd.B);
			//next through to name of value
			fscanf(mtlFile, "%s", buffer);
			//actual values
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].ka.R);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].ka.G);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].ka.B);
			//next through name of value
			fscanf(mtlFile, "%s", buffer);
			//actual values
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].tf.R);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].tf.G);
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].tf.B);
			//next through name of value
			fscanf(mtlFile, "%s", buffer);
			//actual value
			fscanf(mtlFile, "%f", &object.mtlList[mtlIn].Ni);
			//increment mtllistcount
			mtlIn++;
		}
	}
	//close mtlfile
	fclose(mtlFile);
	//start currentmtlindex as 0 - will be used to track current "pen colour" so to speak. Obj file changes mtl for sets of faces
	int currentMtlIndex = 0;
	//read ahead
	fscanf(objFile, "%s", buffer);
	//while not in end of objfile
	while (!feof(objFile))
	{
		//if read string matches one of these keys
		//NOTE: Could use case switch instead for this
		if (strcmp(buffer, "v") == 0)
		{
			//vertex data, read into correct slots
			fscanf(objFile, "%f %f %f", &object.vList[vIn].x, &object.vList[vIn].y, &object.vList[vIn].z);
			//increase vertex in count
			vIn++;
		}
		if (strcmp(buffer, "vt") == 0)
		{
			//vertex texture data, read into correct slots
			fscanf(objFile, "%f %f", &object.vtList[vtIn].x, &object.vtList[vtIn].y);
			//increase vertex texure in count
			vtIn++;
		}
		if (strcmp(buffer, "vn") == 0)
		{
			//vertex normal data, read into correct slots
			fscanf(objFile, "%f %f %f", &object.vnList[vnIn].x, &object.vnList[vnIn].y, &object.vnList[vnIn].z);
			//increase vertex normal in count
			vnIn++;
		}
		if (strcmp(buffer, "usemtl") == 0)
		{
			//Changing currently used mtl
			//set up str to hold matl name to find in mtl list
			char searchfor[50];
			//scan in mtl name into searchfor
			fscanf(objFile, "%s", searchfor);
			//start index to search at
			int searchingindex = 0;
			//search for all mtl read in during mtl file
			while (searchingindex < mtlIn)
			{
				//check if search critera matches mtl name at current search index
				if (strcmp(object.mtlList[searchingindex].mtlName, searchfor) == 0)
				{
					//if it does, current search index becomes current mtl index to add to faces mtlindex
					currentMtlIndex = searchingindex;
				}
				//increase searching index
				searchingindex++;
			}
		}
		if (strcmp(buffer, "f") == 0)
		{
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//TODO!!!!! ADAPT FACE STRUCTURE AND FACE RECOGNITION IN OBJREADER TO ALLOW FOR TRIS AS WELL AS QUADS!!!!!!!!!//
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			object.fList[fIn].mtlIndex = currentMtlIndex;
			//read face line in (eg. 1/1/1)
			fscanf(objFile, "%s", buffer);
			//create split string, split new str buffer by token "/"
			char * split = strtok(buffer, "/");
			//parse split into int and place into flist.vertex1
			object.fList[fIn].vertex1 = atoi(split) -1;
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vnormal1
			object.fList[fIn].vNormal1 = atoi(split) -1 ; 
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vtexture1
			object.fList[fIn].vTexture1 = atoi(split) -1 ;
			//read next set in (should be at white space, moving on to second set of a/b/c)
			fscanf(objFile, "%s", buffer);
			//pass in buffer as new str to split, split along to next token
			split = strtok(buffer, "/");
			//parse split into int and place into flist.vertex2
			object.fList[fIn].vertex2 = atoi(split) -1;
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vnormal1
			object.fList[fIn].vNormal2 = atoi(split) -1;
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vtexture1
			object.fList[fIn].vTexture2 = atoi(split) -1;
			//read next set in (should be at white space, moving on to third set of a/b/c)
			fscanf(objFile, "%s", buffer);
			//pass in buffer as new str to split, split along to next token
			split = strtok(buffer, "/");
			//parse split into int and place into flist.vertex3
			object.fList[fIn].vertex3 = atoi(split) -1;
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vnormal3
			object.fList[fIn].vNormal3 = atoi(split) -1;
			//split along to next token
			split = strtok(NULL, "/");
			//parse split into int and place into flist.vtexture1
			object.fList[fIn].vTexture3 = atoi(split) -1;
			fIn++;
			//read next set in (should be at white space, moving on to last set of a/b/c)
			fscanf(objFile, "%s", buffer);

			//If buffer is not any of these, is fourth corner. Change that quad into a tri.
			if (strcmp(buffer, "f") != 0 && strcmp(buffer, "usemtl") != 0 && strcmp(buffer, "g") != 0 && !feof(objFile))
			{
				object.fList[fIn].mtlIndex = currentMtlIndex;
				//borrowing points 2 and 3 for points 1 and 2 here
				object.fList[fIn].vertex1 = object.fList[fIn - 1].vertex1;
				object.fList[fIn].vNormal1 = object.fList[fIn - 1].vNormal1;
				object.fList[fIn].vTexture1 = object.fList[fIn -1].vTexture1;

				object.fList[fIn].vertex2 = object.fList[fIn - 1].vertex3;
				object.fList[fIn].vNormal2 = object.fList[fIn - 1].vNormal3;
				object.fList[fIn].vTexture2 = object.fList[fIn -1].vTexture3;

				//pass in buffer as new str to split, split along to next token
				split = strtok(buffer, "/");
				//parse split into int and place into flist.vertex4
				object.fList[fIn].vertex3 = atoi(split) - 1;
				//split along to next token
				split = strtok(NULL, "/");
				//parse split into int and place into flist.vnormal4
				object.fList[fIn].vNormal3 = atoi(split) - 1;
				//split along to next token
				split = strtok(NULL, "/");
				//parse split into int and place into flist.vtexture4
				object.fList[fIn].vTexture3 = atoi(split) - 1;
				//tell program one more face has been done
				fIn++;
			}
			else
			{
				didRead = true;
			}



			
		}
		if (didRead)
		{
			didRead = false;
		}
		else
		{
			//read next data
			fscanf(objFile, "%s", buffer);
		}
	}
	//close objfile
	fclose(objFile);
	//return object
	return object;
}
