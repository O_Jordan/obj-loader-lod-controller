#pragma once
class material
{
private:

public:
	material();
	~material();

	struct Kd
	{
		float R;
		float G;
		float B;
	};

	struct Ka
	{
		float R;
		float G;
		float B;
	};

	struct Tf
	{
		float R;
		float G;
		float B;
	};

	Kd kd;
	Ka ka;
	Tf tf;
	float Ni;
	int illum;

	char mtlName[50];
};