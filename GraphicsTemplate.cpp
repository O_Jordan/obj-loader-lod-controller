// GraphicsTemplate.cpp
// 
//////////////////////////////////////////////////////////////////////////////////////////
// includes 
//////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <time.h> //http://www.cplusplus.com/reference/ctime/time/
#include "comlpexOBJ.h"
//////////////////////////////////////////////////////////////////////////////////////////
// externals 
/////////////////////////////////////////////////////////////////////////////////////////

gameObject obj1(1,1,1,1,1);
gameObject obj2(1, 1, 1, 1, 1);
objreader ObjReader;
char * filePath = "myObjFile2.obj";

//for easier position storage
struct pos {
	float x;
	float y;
	float z;
};

//small function to check distance between two points
float checkDist(pos point1, pos point2)
{
	return sqrt((point2.x - point1.x)*(point2.x - point1.x) + (point2.y - point1.y)*(point2.y - point1.y) + (point2.z - point1.z)*(point2.z - point1.z));
}

//initialisations
void init(void);
void display(void);
void drawObj(gameObject obj);
void randomiseObjects();
//cap on number of objects allowed in scene
const int MAX_OBJ = 100;
//time clocks, used for fps
tm *ltm;
time_t now;
int secondsnow;
//fps store
int FPS;

//complex object, and arroy of complexobjects
complexOBJ Rocket;
complexOBJ objArray[MAX_OBJ];
//for rotations
int theta;

float camDist = 15.0f;
float up = 1.0f;

float newX = 0.0f;
float newY = 0.0f;
float newZ = 0.0f;

float camX = 0.0f;
float camY = 0.0f;
float camZ = 0.0f;

pos camera;
pos camLookat;
bool rotateCam = false;
//hack used to speed or slow rotation, along with rotate frames (number of frames between each rotate
int hack = 0;
int rotateFrames = 100;
//not fully implemented, allows controls to switch to change camera position rather than obj
bool camSwitch = false;
//controls how much objects move
float posChangeScale = 0.01f;
//changes lod threshold distances
float lodValue = 0;
//counts number of objects in scene
float numObjects = 0;
//prototype for fps function
void runFPS();

//controls - each do things to manipulate scene, openGL function
void keyboardread(unsigned char key, int x, int y)
{
	switch (key) {
		//move positions
	case 'j':
		if (camSwitch)
		{
			camX = camX + posChangeScale;
		}
		else
		{
			newX = newX + posChangeScale;
		}
		
		glutPostRedisplay();
		break;
	case 'J':
		if (camSwitch)
		{
			camX = camX + posChangeScale;
		}
		else
		{
			newX = newX + posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'l':
		if (camSwitch)
		{
			camX = camX - posChangeScale;
		}
		else
		{
			newX = newX - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'L':
		if (camSwitch)
		{
			camX = camX - posChangeScale;
		}
		else
		{
			newX = newX - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'u':
		if (camSwitch)
		{
			camY = camY - posChangeScale;
		}
		else
		{
			newY = newY - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'U':
		if (camSwitch)
		{
			camY = camY - posChangeScale;
		}
		else
		{
			newY = newY - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'o':
		if (camSwitch)
		{
			camY = camY + posChangeScale;
		}
		else
		{
			newY = newY + posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'O':
		if (camSwitch)
		{
			camY = camY + posChangeScale;
		}
		else
		{
			newY = newY + posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'k':
		if (camSwitch)
		{
			camZ = camZ - posChangeScale;
		}
		else
		{
			newZ = newZ - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'K':
		if (camSwitch)
		{
			camZ = camZ - posChangeScale;
		}
		else
		{
			newZ = newZ - posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'i':
		if (camSwitch)
		{
			camZ = camZ + posChangeScale;
		}
		else
		{
			newZ = newZ + posChangeScale;
		}

		glutPostRedisplay();
		break;
	case 'I':
		if (camSwitch)
		{
			camZ = camZ + posChangeScale;
		}
		else
		{
			newZ = newZ + posChangeScale;
		}

		glutPostRedisplay();
		break;
		//change movement scalar
	case ']':
		posChangeScale = posChangeScale + 0.01;
		printf("POSITION SCALAR: %f\n", posChangeScale);
		break;
	case '[':
		posChangeScale = posChangeScale - 0.01;
		printf("POSITION SCALAR: %f\n", posChangeScale);
		break;
		//increase objs
	case '.':
		if (numObjects < MAX_OBJ)
		{
			numObjects++;
			cout << "OBJECTS: " << numObjects << "\n";
			break;
		}
		else
		{
			break;
		}
		//decrease objs
	case ',':
		numObjects--;
		cout << "OBJECTS: " << numObjects << "\n";
		break;
		//increase LOD thresholds
	case 't':
		lodValue = lodValue + 10;
		cout << "LOD VALUE: " << lodValue << "\n";
		break;
	case 'T':
		lodValue = lodValue + 10;
		cout << "LOD VALUE: " << lodValue << "\n";
		break;
		//decrease LOD thresholds
	case 'g':
		lodValue = lodValue - 10;
		cout << "LOD VALUE: " << lodValue << "\n";
		break;
	case 'G':
		lodValue = lodValue + 10;
		cout << "LOD VALUE: " << lodValue << "\n";
		break;
		//scatter objs
	case 'p':
		randomiseObjects();
		break;
	case 'P':
		randomiseObjects();
		break;
		//slap that camera at origin and begin rotation
	case '#':
		camera.x = 0;
		camera.y = 0;
		camera.z = 0;
		rotateCam = true;
		break;
		//change rotation speed
	case '=':
		hack = 0;
		rotateFrames++;
		break;
	case '-':
		if (rotateFrames > 10)
		{
			hack = 0;
			rotateFrames--;
		}
		
		
	}
}


//opengl init, set up scene
void init(void)
{
	GLfloat mat_specular[] = { 1, 1, 1.0, 1.0 };
	GLfloat mat_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_ambient[] = { 0.8, 0.8, 0.8, 1.0 };
	GLfloat mat_shininess[] = { 12.8 };

	GLfloat light_position[] = { camX, camY, camZ, 0.0 };

	glClearColor(0.0, 0.0, 0.0, 0.0);  //reset "empty" background colour
	glShadeModel(GL_SMOOTH);

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);


	glutInitDisplayMode(GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);

}
//draw obj, using material data
void drawObj(gameObject obj, float x, float y, float z)
{
	//for every face in obj
	for (int i = 0; i < obj.fSize; i++)
	{
		//draw face, position i
		glBegin(GL_TRIANGLES);
		glColor3f(obj.mtlList[obj.fList[i].mtlIndex].kd.R, obj.mtlList[obj.fList[i].mtlIndex].kd.G, obj.mtlList[obj.fList[i].mtlIndex].kd.B);
		glVertex3f(obj.vList[obj.fList[i].vertex1].x + x, obj.vList[obj.fList[i].vertex1].y + y, obj.vList[obj.fList[i].vertex1].z + z);
		glVertex3f(obj.vList[obj.fList[i].vertex2].x + x, obj.vList[obj.fList[i].vertex2].y + y, obj.vList[obj.fList[i].vertex2].z + z);
		glVertex3f(obj.vList[obj.fList[i].vertex3].x + x, obj.vList[obj.fList[i].vertex3].y + y, obj.vList[obj.fList[i].vertex3].z + z);
		glEnd();
	}
}
//function to scatter objects
void randomiseObjects()
{
	for (int i = 0; i < numObjects; i++)
	{
		if (i < MAX_OBJ)
		{
			objArray[i].X = (rand() % 201) - 100;
			objArray[i].Y = rand() % 6;
			objArray[i].Z = (rand() % 201) - 100;
		}
	}
}
//draw obj, but with faces alternating colour. Use to see polys easier. Contains LOD management - Decouple?
void drawObjTest(complexOBJ * obj, float xOffset, float yOffset, float zOffset)
{
	bool red = true;
	//for every face in obj
	gameObject toDraw;
	
	
	pos objpos;
	objpos.x = xOffset;
	objpos.y = yOffset;
	objpos.z = zOffset;
	//lod management
	if (checkDist(camera, objpos) < lodValue + 35 && obj->stage == 1)
	{
		obj->stage = 0;
	}
	else if (checkDist(camera, objpos) > lodValue + 40 && obj->stage == 0)
	{
		obj->stage = 1;
	}
	else if (checkDist(camera, objpos) < lodValue + 45 && obj->stage == 2)
	{
		obj->stage = 1;
	}
	else if (checkDist(camera, objpos) > lodValue + 50 && obj->stage == 1)
	{
		obj->stage = 2;
	}
	else if (checkDist(camera, objpos) < lodValue + 55 && obj->stage == 3)
	{
		obj->stage = 2;
	}
	else if (checkDist(camera, objpos) > lodValue + 60 && obj->stage == 2)
	{
		obj->stage = 3;
	}
	else if (checkDist(camera, objpos) < lodValue + 65 && obj->stage == 4)
	{
		obj->stage = 3;
	}
	else if (checkDist(camera, objpos) > lodValue + 70 && obj->stage == 3)
	{
		obj->stage = 4;
	}
	//select appropriate res obj
	if (obj->stage == 0)
	{
		toDraw = obj->main;
	}
	else if (obj->stage == 1)
	{
		toDraw = obj->LOD1;
	}
	else if (obj->stage == 2)
	{
		toDraw = obj->LOD2;
	}
	else if (obj->stage == 3)
	{
		toDraw = obj->LOD3;
	}
	else
	{
		toDraw = obj->LOD4;
	}
	for (int i = 0; i < toDraw.fSize; i++)
	{
		//draw face, position i
		glBegin(GL_TRIANGLES);
		if (red)
		{
			glColor3f(1, 0, 0);
			red = false;
		}
		else
		{
			glColor3f(0, 0, 1);
			red = true;
		}
		glVertex3f(toDraw.vList[toDraw.fList[i].vertex1].x + xOffset, toDraw.vList[toDraw.fList[i].vertex1].y + yOffset, toDraw.vList[toDraw.fList[i].vertex1].z + zOffset);
		glVertex3f(toDraw.vList[toDraw.fList[i].vertex2].x + xOffset, toDraw.vList[toDraw.fList[i].vertex2].y + yOffset, toDraw.vList[toDraw.fList[i].vertex2].z + zOffset);
		glVertex3f(toDraw.vList[toDraw.fList[i].vertex3].x + xOffset, toDraw.vList[toDraw.fList[i].vertex3].y + yOffset, toDraw.vList[toDraw.fList[i].vertex3].z + zOffset);
		glEnd();
	}
}
//opengl display
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0, 1.0, 1.0);
	glLoadIdentity();
	runFPS();
	//rotating camera, just changes look at position (incorrect?)
	if (rotateCam)
	{
		camLookat.x = sin(float(theta)*(0.0175));
		camLookat.y = 0;
		camLookat.z = cos(float(theta)*(0.0175));
		//if correct frame to rotate
		if (hack == rotateFrames)
		{
			hack = 0;
			theta = (theta + 1) % 360;
		}
		else
		{
			hack++;
		}
		
	}
	//set camera
	gluLookAt(camera.x, camera.y, camera.z, camLookat.x, camLookat.y, camLookat.z, 0.0, 1.0, 0.0);
	glPushMatrix();
	//draw all objects
	for (int i = 0; i < numObjects; i++)
	{
		if (i < MAX_OBJ)
		{
			drawObjTest(&objArray[i], objArray[i].X + newX, objArray[i].Y+ newY, objArray[i].Z+newZ);
		}
		
	}
	glutSwapBuffers();
}
//opengl reshape
void reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//frustum does your clipping
	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 500.0);

	glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv)
{
	
	//initialise - NOT IN TXT, UNDERSTAND HOW TO WORK IT, TIME CONSTRAINTS GOT ME
	//note: switch this to txt loading, in complexObject class - txt contains objs for each level,
	//read in each file name from txt and use those file names to load each LOD obj
	Rocket.main = ObjReader.readOBJfile("bigObject.obj");
	Rocket.LOD1 = ObjReader.readOBJfile("bigObject2.obj");
	Rocket.LOD2 = ObjReader.readOBJfile("bigObject3.obj");
	Rocket.LOD3 = ObjReader.readOBJfile("bigObject4.obj");
	Rocket.LOD4 = ObjReader.readOBJfile("bigObject5.obj");
	//seed for random numbers
	srand((unsigned)time(0));
	//initial camera positionings
	camera.x = 0;
	camera.y = 5;
	camera.z = 10;

	camLookat.x = 0;
	camLookat.y = 0;
	camLookat.z = -10;

	//set each OBJ in array to blank position copy of OBJ
	for (int z = 0; z < MAX_OBJ; z++)
	{
		objArray[z] = Rocket;
	}

	printf("\n\n\n/////////////////////////////////////////////////////////////\nHELLO! USE . TO INCREASE OBJECTS AND , TO DECRASE. MAX 50 \n USE T TO INCREASE LOD THRESHOLDS AND G TO DECREASE \n USE # TO START CAMERA ROTATION \n USE I AND K TO MOVE THE OBJECT TOWARDS AND AWAY!\nUSE J AND L TO MOVE THE OBJECT LEFT AND RIGHT!\nUSE U AND O TO MOVE THE OBJECT UP AND DOWN!\nUSE [ AND ] TO CHANGE SPEED OF OBJECT MOVEMENT!\n/////////////////////////////////////////////////////////////\n");
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(750, 750);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("A Superb Cube I Hope");
	init();
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboardread);
	now = time(0);
	ltm = localtime(&now);
	secondsnow = ltm->tm_sec;
	FPS = 0;
	glutIdleFunc(display);
	glutMainLoop();
	return 0;
	
}
//fps counter - NOTE: not an fully accurate FPS counter - counts frames drawn in last second. 
//True FPS would need me to measure time to draw frame and average out over a second.
void runFPS()
{
	int newseconds;
	now = time(0);
	ltm = localtime(&now);
	newseconds = ltm->tm_sec;
	if (newseconds == secondsnow)
	{
		FPS++;
	}
	else
	{
		cout << "FPS: " << FPS << "\n";
		FPS = 0;
		secondsnow = newseconds;
	}

}




